Microlly - Web Python Project by LACROIX Loic & ZARROU Wassim

Pour le bon fonctionnement du blog Microlly, veuillez exécuter les commandes suivantes : 

`pipenv shell`

`pipenv install -r requirements.txt`

`flask run`

-----------------------------------------------------------------------------------
For the proper functioning of the Microlly blog, please run the following commands :

`pipenv shell`

`pipenv install -r requirements.txt`

`flask run`